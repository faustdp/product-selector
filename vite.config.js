import { defineConfig } from 'vite'
import precss from 'precss'
import reactRefresh from '@vitejs/plugin-react-refresh'

export default defineConfig({
  plugins: [reactRefresh()],
  css: {
    postcss: {
      plugins: [precss()],
    },
  },
  server: {
    open: true,
  },
  esbuild: {
    jsxInject: `import React from 'react'`,
  },
  build: {
    sourcemap: true,
    brotliSize: false,
    terserOptions: {
      compress: {
        pure_funcs: ['console.log', 'console.clear'],
      },
    },
  },
})
