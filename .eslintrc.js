module.exports = {
  env: {
    browser: true,
  },
  extends: ['airbnb-base', 'plugin:react/recommended', 'plugin:prettier/recommended'],
  settings: {
    react: {
      version: 'latest',
    },
    'import/resolver': {
      node: {
        extensions: ['.js', '.jsx'],
      },
    },
  },
  plugins: ['prettier', 'react', 'react-hooks'],
  ignorePatterns: ['/public', '/build', '/dist'],
  parserOptions: {
    ecmaFeatures: {
      jsx: true,
    },
    ecmaVersion: 12,
    sourceType: 'module',
  },
  rules: {
    'max-len': [1, 120],
    'no-console': 0,
    'no-unused-vars': [
      1,
      {
        argsIgnorePattern: '[_]',
        varsIgnorePattern: '[_]',
        ignoreRestSiblings: true,
      },
    ],
    'import/no-extraneous-dependencies': ['error', { devDependencies: true }],
    'import/extensions': ['error', 'ignorePackages', { js: 'never', jsx: 'never' }],
    'import/prefer-default-export': 'off',
    'react/prop-types': 0,
    'react/react-in-jsx-scope': 'off',
    'react-hooks/rules-of-hooks': 'error',
    'react-hooks/exhaustive-deps': 'warn',
    'consistent-return': 0,
    'no-param-reassign': [2, { props: false }],
  },
}
