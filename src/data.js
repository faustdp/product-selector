const breeds = {
  Dog: [
    'affenpinscher',
    'Afghan hound',
    'Airedale terrier',
    'Akita',
    'Alaskan Malamute',
    'American Staffordshire terrier',
    'American water spaniel',
    'Australian cattle dog',
    'Australian shepherd',
    'Australian terrier',
    'basenji',
    'basset hound',
    'beagle',
    'bearded collie',
    'Bedlington terrier',
    'Bernese mountain dog',
    'bichon frise',
    'black and tan coonhound',
    'bloodhound',
    'border collie',
    'border terrier',
    'borzoi',
    'Boston terrier',
    'bouvier des Flandres',
    'boxer',
    'briard',
    'bullmastiff',
    'cairn terrier',
    'Canaan dog',
    'Chesapeake Bay retriever',
    'Chihuahua',
    'Chinese crested',
    'collie',
    'curly-coated retriever',
    'dachshund',
    'Dalmatian',
    'Doberman pinscher',
    'English cocker spaniel',
    'English setter',
    'flat-coated retriever',
    'fox terrier',
    'foxhound',
    'French bulldog',
    'German shepherd',
    'German shorthaired pointer',
    'German wirehaired pointer',
    'Irish wolfhound',
    'Jack Russell terrier',
    'Kooikerhondje',
    'Koolie',
    'Leonberger',
    'Norrbottenspets',
    'Otterhound',
    'Skye terrier',
    'Staffordshire bull terrier',
    'soft-coated wheaten terrier',
    'Sussex spaniel',
    'spitz',
    'Tibetan terrier',
    'vizsla',
    'Weimaraner',
    'Welsh terrier',
    'West Highland white terrier',
    'whippet',
    'Xoloitzcuintle',
    'Yorkshire terrier',
  ],
  Cat: [
    'Abyssinian',
    'Aegean',
    'American Bobtail',
    'American Wirehair',
    'Aphrodite Giant',
    'Arabian Mau',
    'Balinese',
    'Bambino',
    'Bengal Cats',
    'Birman',
    'Bombay',
    'Brazilian Shorthair',
    'British Shorthair',
    'Burmese',
    'Burmilla',
    'California Spangled',
    'Chantilly-Tiffany',
    'Chartreux',
    'Chausie',
    'Chinese Li Hua',
    'Colorpoint Shorthair',
    'Cornish Rex',
    'Cymric',
    'Desert Lynx',
    'Devon Rex',
    'Egyptian Mau',
    'European Shorthair',
    'Foldex',
    'German Rex',
    'Havana Brown',
    'Highlander',
    'Japanese Bobtail',
    'Khao Manee',
    'Korat',
    'LaPerm',
    'Maine Coon',
    'Manx',
    'Napoleon',
    'Nebelung',
    'Ocicat',
    'Oriental Bicolor',
    'Persian',
    'Peterbald',
    'Ragamuffin',
    'Ragdoll Cats',
    'Russian Blue',
    'Selkirk Rex',
    'Serengeti',
    'Siamese Cat',
    'Siberian',
    'Singapura',
    'Snowshoe',
    'Sokoke',
    'Somali',
    'Thai Lilac',
    'Tonkinese',
    'Toyger',
    'Turkish Angora',
    'Turkish Van',
    'York Chocolate',
  ],
}

export default breeds
