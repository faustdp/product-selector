import ReactDOM from 'react-dom'
import './index.css'
import App from './App'

console.clear()

ReactDOM.render(<App />, document.getElementById('root'))
