/* eslint-disable */
import { useRef, useEffect, useCallback } from 'react'

function useIsMounted() {
  const ref = useRef(false);
  useEffect(() => {
    ref.current = true;
    return () => {
      ref.current = false;
    };
  }, []);
  return useCallback(() => ref.current, []);
}

function throttle(func, ms, leading = true, trailing = true) {
  let timer = null,
    next = 0,
    timeDiff;
  function throttled(...args) {
    const now = Date.now();
    timeDiff = next - now;
    if (timer) return;
    if (leading && timeDiff <= 0) {
      next = now + ms;
      return func.call(this, ...args);
    }
    if (trailing) {
      timer = setTimeout(
        () => {
          func.call(this, ...args);
          next = Date.now() + ms;
          timer = null;
        },
        leading ? next - now : ms,
      );
    }
  }
  throttled.cancel = function () {
    clearTimeout(timer);
    timer = null;
  };
  return throttled;
}

function animate(params) {
  let { draw, paused = false, reversed = false } = params
  const { duration = 300, timing = (val) => val, cb = () => {} } = params
  let start = performance.now()
  let prevProgress = reversed ? 1 : 0
  let currProgress = 0
  let id = null
  let timeScale = 1

  function anim(time) {
    if (paused) return anim
    const diff = Math.max(time - start, 1)
    const progress = (diff / duration) * timeScale
    currProgress = reversed ? prevProgress - progress : prevProgress + progress
    if (currProgress > 1) currProgress = 1
    if (currProgress < 0) currProgress = 0
    const timeFraction = timing(currProgress)
    draw(timeFraction)
    if (currProgress === 1 || currProgress === 0) {
      stopAnim(true)
      cb(anim)
      return anim
    }
    id = requestAnimationFrame(anim)
  }

  function stopAnim(isFinished) {
    paused = true
    prevProgress = isFinished ? (reversed || timeScale < 0 ? 1 : 0) : currProgress
    cancelAnimationFrame(id)
    return anim
  }

  function startAnim(isPaused = false) {
    if (timeScale === 0) return
    if (reversed && currProgress === 1 && timeScale < 0) prevProgress = 0
    paused = isPaused
    start = performance.now()
    id = requestAnimationFrame(anim)
    return anim
  }

  anim.play = () => (paused ? startAnim() : anim)

  anim.pause = () => (!paused ? stopAnim() : anim)

  anim.toggle = () => (paused ? startAnim() : stopAnim())

  anim.restart = (func, isPaused = false) => {
    stopAnim()
    if (func) {
      draw = func
    }
    timeScale = 1
    reversed = params.reversed ?? false
    prevProgress = reversed ? 1 : 0
    return startAnim(isPaused)
  }

  anim.paused = () => paused

  anim.reversed = () => !!((reversed && timeScale >= 0) || (!reversed && timeScale < 0))

  anim.reverse = () => {
    reversed = !reversed
    stopAnim()
    return startAnim()
  }

  anim.timeScale = (val) => {
    if (val === undefined) return timeScale
    timeScale = +val
    if (timeScale === 0) {
      return stopAnim()
    }
    if (!paused) {
      start = performance.now()
      prevProgress = currProgress
    }
    return anim
  }

  id = requestAnimationFrame(anim)

  return anim
}

const Circular = {
  In: (k) => 1 - Math.sqrt(1 - k * k),
  Out: (k) => Math.sqrt(1 - --k * k),
  InOut: (k) => ((k *= 2) < 1 ? -0.5 * (Math.sqrt(1 - k * k) - 1) : 0.5 * (Math.sqrt(1 - (k -= 2) * k) + 1)),
};

export { useIsMounted, throttle, animate, Circular }
