import { useState, useEffect, useLayoutEffect, useCallback, useRef } from 'react'
import { useIsMounted, throttle, animate, Circular } from './helpers'
import breeds from './data'
import './App.css'

const resultName = '[result]'

const vowels = ['a', 'e', 'i', 'o', 'u', 'y']

const fadeDur = 200

const transformDur = 300

const progressDur = 350

const containerDur = fadeDur + transformDur

const topPaddingVh = 30

const vhDiff = 2.3

function Progress({ percentage }) {
  const [animPercentage, animPercentageSet] = useState(0)
  const animPercentageRef = useRef(0)
  const animRef = useRef(
    animate({
      draw: () => {},
      duration: progressDur,
      timing: Circular.Out,
      paused: true,
    }),
  )

  useEffect(() => {
    if (percentage === 0 && animPercentageRef.current === 0) return

    const intPercentage = Math.trunc(percentage * 100)
    const prevPercentage = animPercentageRef.current
    const anim = animRef.current

    anim.restart((progress) => {
      const result = Math.round((intPercentage - prevPercentage) * progress + prevPercentage)
      if (result !== animPercentageRef.current) {
        animPercentageRef.current = result
        animPercentageSet(result)
      }
    })

    return () => {
      anim?.pause()
    }
  }, [percentage])

  return (
    <div className="App-progress" style={{ '--progress-bar': percentage }}>
      <p className="App-progress_percentage">{animPercentage}% Completed</p>
      <div className="App-progress_bar" />
    </div>
  )
}

function ResultsBtn({ onClick, currIndex }) {
  const resultsRef = useRef(null)
  const animElRect = useRef()
  const isMounted = useIsMounted()

  useLayoutEffect(() => {
    const anim = resultsRef.current.animate(
      {
        transform: ['scale(.4)', 'scale(1)'],
        opacity: [0, 1],
      },
      { duration: transformDur, fill: 'forwards', easing: 'cubic-bezier(0.4, 1.1, 0.89, 1.3)' },
    )

    anim.onfinish = () => {
      animElRect.current = resultsRef.current.getBoundingClientRect().top + window.pageYOffset
    }

    return () => {
      anim.cancel()
    }
  }, [])

  useLayoutEffect(() => {
    if (!isMounted()) return

    const elTop = resultsRef.current.getBoundingClientRect().top + window.pageYOffset
    const animDiff = animElRect.current - elTop
    const anim = resultsRef.current.animate(
      {
        transform: [`translateY(${animDiff}px)`, 'translateY(0)'],
      },
      { duration: transformDur - 50, fill: 'forwards', easing: 'cubic-bezier(0.19, 1, 0.22, 1)' },
    )

    animElRect.current = elTop

    return () => {
      anim?.cancel()
    }
  }, [currIndex, isMounted])

  return (
    <div className="App-results" onClick={onClick} ref={resultsRef}>
      <button className="App-results_btn">See Results</button>
      <p className="App-results_desc">*None of recipes includes corn, wheat, soy and GMOs.</p>
    </div>
  )
}

function Option({ name, handleSubmit, desc, isActive, type }) {
  function onClick() {
    handleSubmit(name)
  }

  return (
    <button
      className={`question-option ${type === 'polar' ? 'question-option--polar' : ''} ${isActive ? 'is-active' : ''}`}
      onClick={onClick}
    >
      {name}
      {desc && <span>{desc}</span>}
    </button>
  )
}

function Suggestion({ name, handleSubmit, inputRef, suggestionsSet }) {
  function handleClick() {
    inputRef.value = name
    suggestionsSet([])
    handleSubmit(name)
  }

  return <span onClick={handleClick}>{name}</span>
}

function Question({
  info,
  question,
  type,
  options,
  cb,
  result,
  index,
  isFinished,
  totalIndexSet,
  renderIndexSet,
  currIndex,
  currIndexSet,
  animal,
}) {
  const [answer, answerSet] = useState('')
  const [isAnswered, isAnsweredSet] = useState(false)
  const [showTip, showTipSet] = useState(false)
  const [questionIsVisible, questionIsVisibleSet] = useState(true)
  const [suggestions, suggestionsSet] = useState([])
  const inputRef = useRef(null)
  const resultRef = useRef(null)
  const questionRef = useRef(null)
  const animElTop = useRef()
  const isMounted = useIsMounted()

  const showQuestion = useCallback(() => {
    isAnsweredSet(false)
    suggestionsSet([])
    questionIsVisibleSet(true)
  }, [])

  const handleResultClick = useCallback(() => {
    const anim = resultRef.current.animate(
      {
        opacity: [1, 0],
        transform: ['scale(1)', 'scale(0.85)'],
      },
      { duration: fadeDur, fill: 'forwards', easing: 'cubic-bezier(0.25, 0.46, 0.45, 0.94)' },
    )
    anim.onfinish = () => {
      showQuestion()
      currIndexSet({ index })
    }
  }, [showQuestion, index, currIndexSet])

  const handleSubmit = useCallback(
    (val) => {
      totalIndexSet((prev) => Math.max(prev, index + 1))
      isAnsweredSet(true)
      answerSet(val)
      if (cb) cb(val)
    },
    [cb, totalIndexSet, index],
  )

  const handleChange = useCallback((e) => {
    const value = e.target.value.trim()
    if (value) {
      showTipSet(true)
    } else {
      showTipSet(false)
    }
  }, [])

  const handleKeyDown = useCallback(
    (e) => {
      if (e.key !== 'Enter') return
      const value = e.target.value.trim()
      if (value) handleSubmit(value)
    },
    [handleSubmit],
  )

  const handleSuggestions = useCallback(
    (e) => {
      const value = e.target.value.trim()
      const fixedValue = value.replaceAll(/(\[|\||\^|\$|\.|\?|\*|\+|\(|\)|\\)/g, '\\$&')
      const reg = new RegExp(`(\\s|^)${fixedValue}`, 'i')
      const results = value ? breeds[animal]?.filter((el) => reg.test(el)).slice(0, 5) : []
      suggestionsSet(results)
    },
    [animal],
  )

  useLayoutEffect(() => {
    if (!isFinished && index === 0) {
      isAnsweredSet(false)
      answerSet('')
      questionIsVisibleSet(true)
    }
  }, [index, isFinished])

  useLayoutEffect(() => {
    if (!isAnswered || !questionRef.current) return

    const anim = questionRef.current.animate(
      {
        opacity: [1, 0],
      },
      { duration: fadeDur, fill: 'forwards', easing: 'cubic-bezier(0.25, 0.46, 0.45, 0.94)' },
    )

    anim.onfinish = () => {
      questionIsVisibleSet(false)
      renderIndexSet((prev) => {
        if (prev > index + 1) currIndexSet({ index })
        return Math.max(prev, index + 2)
      })
    }

    return () => {
      anim?.cancel()
    }
  }, [isAnswered, renderIndexSet, currIndexSet, index])

  useLayoutEffect(() => {
    const animEl = questionRef.current || resultRef.current
    if (!animEl) return

    let anim

    if (questionIsVisible) {
      anim = animEl.animate(
        {
          opacity: [0, 1],
          transform: ['translateY(25px)', 'translateY(0)'],
        },
        { duration: transformDur, fill: 'forwards', easing: 'cubic-bezier(0.39, 0.575, 0.565, 1)' },
      )
    } else {
      anim = animEl.animate(
        {
          opacity: [0, 1],
          transform: ['translateY(15px)', 'translateY(0)'],
        },
        { duration: fadeDur, fill: 'forwards', easing: 'cubic-bezier(0.39, 0.575, 0.565, 1)' },
      )
    }

    anim.onfinish = () => {
      animElTop.current = animEl.getBoundingClientRect().top + window.pageYOffset
    }

    return () => {
      anim?.cancel()
    }
  }, [questionIsVisible])

  useLayoutEffect(() => {
    if (!isMounted() || currIndex.index === null || currIndex.index >= index) return

    const animEl = questionRef.current || resultRef.current
    const elTop = animEl.getBoundingClientRect().top + window.pageYOffset
    const animDiff = animElTop.current - elTop

    const anim = animEl.animate(
      {
        transform: [`translateY(${animDiff}px)`, 'translateY(0)'],
      },
      { duration: transformDur - 50, fill: 'forwards', easing: 'cubic-bezier(0.19, 1, 0.22, 1)' },
    )

    animElTop.current = elTop

    return () => {
      anim?.cancel()
    }
  }, [currIndex, index, isMounted])

  useLayoutEffect(() => {
    if (type === 'datalist' && isMounted()) {
      answerSet('')
      showQuestion()
    }
  }, [type, animal, isMounted, showQuestion])

  useEffect(() => {
    if (questionIsVisible && !isAnswered && inputRef.current) {
      if (answer) {
        inputRef.current.value = answer
      }
      inputRef.current.focus()
    }
  }, [questionIsVisible, answer, isAnswered])

  const QuestionEl = () => {
    switch (type) {
      case 'select':
        return '(select one)'
      case 'input':
        return (
          <input
            onChange={handleChange}
            onKeyDown={handleKeyDown}
            type="text"
            className="question-input question-input--name"
            spellCheck={false}
            ref={inputRef}
          />
        )
      case 'datalist':
        return (
          <div className={`question-input_cont ${suggestions.length > 0 ? ' is-open' : ''}`}>
            <input
              onChange={handleSuggestions}
              onKeyDown={handleKeyDown}
              type="text"
              className="question-input question-input--breeds"
              spellCheck={false}
              ref={inputRef}
            />
            {suggestions.length > 0 && (
              <div className="question-input_suggestions">
                {suggestions.map((el) => (
                  <Suggestion
                    key={el}
                    name={el}
                    handleSubmit={handleSubmit}
                    inputRef={inputRef.current}
                    suggestionsSet={suggestionsSet}
                  />
                ))}
              </div>
            )}
          </div>
        )
      default:
        return null
    }
  }

  let text = result || question
  let postfix = ''

  if (isAnswered) {
    const hasPostfix = result?.includes(resultName)
    if (hasPostfix) {
      const postfixIndex = text.indexOf(resultName)
      postfix = text.slice(postfixIndex + resultName.length)
      text = text.slice(0, postfixIndex)
    }
    if (text.endsWith(' a') && vowels.includes(answer.slice(0, 1).toLowerCase())) {
      text += 'n'
    }
  }

  return (
    <>
      {isAnswered && !questionIsVisible && (
        <p className="question-result" onClick={handleResultClick} ref={resultRef}>
          {text}{' '}
          <span>
            {answer}
            {postfix}.
          </span>
        </p>
      )}
      {questionIsVisible && (
        <div className="question-cont" ref={questionRef}>
          {info && <p className="question-info">{info}</p>}
          <div className="question-text">
            {question} {QuestionEl()}
            {showTip && (
              <p className="question-tooltip">
                Press <span>Enter</span>
              </p>
            )}
          </div>
          {options && (
            <div className="question-options">
              {options.map((option) => {
                const name = option.name ?? option
                const isActive = !isAnswered && answer === name
                return (
                  <Option
                    key={name}
                    name={name}
                    handleSubmit={handleSubmit}
                    desc={option.desc}
                    isActive={isActive}
                    type={type}
                  />
                )
              })}
            </div>
          )}
        </div>
      )}
    </>
  )
}

function DribbbleLink() {
  return (
    <a
      href="https://dribbble.com/shots/4067019-Product-Selector-Prototype"
      target="_blank"
      rel="noopener noreferrer"
      className="dribbble-link"
    >
      <img src="https://icons.iconarchive.com/icons/uiconstock/socialmedia/256/Dribbble-icon.png" />
    </a>
  )
}

function App() {
  const [animal, animalSet] = useState('')
  const [animalName, animalNameSet] = useState('')
  const [totalIndex, totalIndexSet] = useState(0)
  const [renderIndex, renderIndexSet] = useState(1)
  const [currIndex, currIndexSet] = useState({ index: null })
  const [data, dataSet] = useState([])
  const contRef = useRef(null)
  const contTop = useRef()
  const animRef = useRef()
  const isScrolled = useRef(false)

  function restartQuiz() {
    totalIndexSet(0)
    renderIndexSet(1)
    animalSet('')
    animalNameSet('')
  }

  useLayoutEffect(() => {
    dataSet([
      {
        info: `Find your pet's perfect meal. It's quick and easy.`,
        question: 'I have a',
        type: 'select',
        options: ['Dog', 'Cat'],
        cb: animalSet,
      },
      {
        question: `My ${animal}'s name is`,
        type: 'input',
        cb: animalNameSet,
      },
      {
        question: `${animalName} is a`,
        type: 'datalist',
      },
      {
        question: `${animalName} is a`,
        type: 'select',
        options: ['Puppy', 'Adult', 'Senior'],
        result: `${animalName} is`,
      },
      {
        question: `${animalName} is about this big`,
        type: 'select',
        options: [
          { name: 'Toy', desc: '1-10 lbs' },
          { name: 'Small', desc: '11-30 lbs' },
          { name: 'Medium', desc: '31-50 lbs' },
          { name: 'Large', desc: '51-70 lbs' },
          { name: 'Giant', desc: '71+ lbs' },
        ],
        result: `${animalName} is ${resultName}-Sized`,
      },
      {
        question: `${animalName} activity level is`,
        type: 'select',
        options: [
          `Less
        Active`,
          'Active',
          `Very
          Active`,
        ],
        result: `${animalName} is`,
      },
      {
        question: `Does ${animalName} have any food allergies or preferences?`,
        type: 'polar',
        options: [
          { name: 'Yes', desc: 'Avoids one or more ingredients.' },
          { name: 'No', desc: 'Chows down on everything.' },
        ],
      },
    ])
  }, [animal, animalName])

  useLayoutEffect(() => {
    const yScroll = window.pageYOffset
    const topOffset = contRef.current.getBoundingClientRect().top + yScroll

    const scrollDiff = document.documentElement.scrollHeight - document.documentElement.clientHeight

    isScrolled.current = (yScroll > 0 && scrollDiff - yScroll <= 1) || scrollDiff === 0

    if (totalIndex > 0 && (yScroll === 0 || !isScrolled.current)) {
      let prevTranslate = 0
      if (animRef.current && animRef.current.playState !== 'finished') {
        const { transform } = window.getComputedStyle(contRef.current)
        prevTranslate = +transform.slice(transform.lastIndexOf(', ') + 2, transform.length - 1)
      }

      if (!contTop.current) {
        contTop.current = topOffset - document.documentElement.clientHeight * 0.01 * vhDiff
      }

      const animDiff = Math.abs(topOffset - contTop.current) + prevTranslate

      animRef.current = contRef.current.animate(
        {
          transform: [`translateY(${animDiff}px)`, 'translateY(0)'],
        },
        { duration: containerDur, fill: 'forwards', easing: 'cubic-bezier(0.32, 0.5, 0.61, 1.07)' },
      )
    }

    contTop.current = topOffset
  }, [totalIndex])

  useLayoutEffect(() => {
    if (isScrolled.current && document.documentElement.scrollHeight > document.documentElement.clientHeight) {
      contRef.current.parentElement.scrollIntoView({ behavior: 'smooth', block: 'end' })
    }
  }, [renderIndex])

  useEffect(() => {
    const calculateRect = throttle(() => {
      contTop.current = contRef.current.getBoundingClientRect().top + window.pageYOffset
    }, 100)

    window.addEventListener('resize', calculateRect)

    const contAnim = animRef.current

    return () => {
      window.removeEventListener('resize', calculateRect)
      contAnim?.cancel()
    }
  }, [])

  const percentage = totalIndex / data.length || 0

  const animDiff = totalIndex * vhDiff

  const isFinished = renderIndex > 1 && renderIndex - 1 === data.length

  return (
    <div className="App" style={{ '--padding-top': `${topPaddingVh - animDiff}vh` }}>
      <div className="App-questions" ref={contRef}>
        {data.slice(0, renderIndex).map((question, ind) => (
          <Question
            key={ind + question.type}
            index={ind}
            isFinished={isFinished}
            totalIndexSet={totalIndexSet}
            renderIndexSet={renderIndexSet}
            currIndex={currIndex}
            currIndexSet={currIndexSet}
            animal={animal}
            {...question}
          />
        ))}
      </div>
      {isFinished && <ResultsBtn onClick={restartQuiz} currIndex={currIndex} />}
      <Progress percentage={percentage} />
      <DribbbleLink />
    </div>
  )
}

export default App
